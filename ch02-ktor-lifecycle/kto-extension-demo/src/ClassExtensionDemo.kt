package org.elu.learning.kotlin.ktor

fun Int.addFive() = this + 5

fun main() {
    val myInt = 5
    println(myInt.addFive())
}
