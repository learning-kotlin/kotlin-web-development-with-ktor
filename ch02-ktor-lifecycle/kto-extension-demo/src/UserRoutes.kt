package org.elu.learning.kotlin.ktor

import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*

fun Routing.userRoutes() {
    get("/user") {
        call.respondText("User1")
    }

    post("/user") {
        call.respondText("The user has been created")
    }
}