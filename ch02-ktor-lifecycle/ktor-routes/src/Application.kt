package org.elu.learning.kotlin.ktor

import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.gson.*
import io.ktor.features.*

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(ContentNegotiation) {
        gson {
        }
    }

    routing {
        trace { application.log.trace(it.buildText()) }
        route("/weather") {
            route("/europe") {
                handle {
                    call.respondText { "The weather in Europe: sunny" }
                }
            }
            route("/asia", HttpMethod.Get) {
                handle {
                    call.respondText { "The weather in Asia: rainy" }
                }
            }
            get("/north-america") {
                call.respondText { "The weather in North America: cold and snowy" }
            }
            route("/africa", HttpMethod.Get) {
                param("name") {
                    handle {
                        val name = call.parameters["name"]
                        call.respondText { "Hello $name! The weather in Africa: hot and dry" }
                    }
                }
                handle {
                    call.respondText { "The weather in Africa: hot and dry" }
                }
            }
            route("/south-america", HttpMethod.Get) {
                header("systemToken", "weatherSystem") {
                    handle {
                        call.respondText { "The weather in South America: hot and humid" }
                    }
                }
            }
        }
    }
}
