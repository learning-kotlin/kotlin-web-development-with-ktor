curl -X POST localhost:8080/weather/europe
echo
curl localhost:8080/weather/europe
echo
curl localhost:8080/weather/asia
echo
curl -X POST localhost:8080/weather/asia # no response since POST method is not implemented
echo
curl localhost:8080/weather/north-america
echo
curl localhost:8080/weather/africa
echo
curl "localhost:8080/weather/africa?name=Edu"
echo
curl localhost:8080/weather/south-america
echo
curl -H "systemToken: weatherSystem" localhost:8080/weather/south-america
