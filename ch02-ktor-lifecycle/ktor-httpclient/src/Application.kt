package org.elu.learning.kotlin.ktor

import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.gson.*
import io.ktor.features.*
import io.ktor.client.*
import io.ktor.client.engine.apache.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.client.request.*
import kotlinx.coroutines.*

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(ContentNegotiation) {
        gson {
        }
    }

    val client = HttpClient(Apache) {
        install(JsonFeature) {
            serializer = GsonSerializer()
        }
    }
    runBlocking {
        // Sample for making a HTTP Client request
        /*
        val message = client.post<JsonSampleClass> {
            url("http://127.0.0.1:8080/path/to/endpoint")
            contentType(ContentType.Application.Json)
            body = JsonSampleClass(hello = "world")
        }
        */
    }

    routing {
        get("/") {
            call.respondText("HELLO WORLD!", contentType = ContentType.Text.Plain)
        }

        get("/spaceship") {
            call.respond(Spaceship("Galactica", 15))
        }

        get("/consume-service") {
            log.info("Consume BEGIN")
//            val result = client.get<ByteArray>("http://localhost:8080/spaceship")
//            log.info("Result: ${String(result)}")
//            call.respondText { String(result) }
//            val result = client.get<String>("http://localhost:8080/spaceship")
//            log.info("Result: $result")
//            call.respondText { result }
            val result = client.get<Spaceship>("http://localhost:8080/spaceship")
            log.info("Result: $result")
            call.respond(result)
            log.info("Consume END")
        }
    }
}

data class Spaceship(val name: String, val fuel: Int)
