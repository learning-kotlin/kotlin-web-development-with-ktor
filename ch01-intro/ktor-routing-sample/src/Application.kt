package org.elu.learn.kotlin.ktor

import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.routing.*
import io.ktor.http.*

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    routing {
        get("/") {
            call.respondText("HELLO WORLD!", contentType = ContentType.Text.Plain)
        }
        var weather = "sunny"
        get("/weather-forecast") {
            call.respondText(weather, contentType = ContentType.Text.Plain)
        }
        post("/weather-forecast") {
            weather = call.receiveText()
            call.respondText("Weather has been set to '$weather'", contentType = ContentType.Text.Plain)
        }
    }
}

