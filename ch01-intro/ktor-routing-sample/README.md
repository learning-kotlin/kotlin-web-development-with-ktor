# Ktor routing sample application

To test access default route use following command:
```
curl http://0.0.0.0:8080
```

It should respond with the string `HELLO WORLD`.

To test newly added route with `GET` method use following command:
```
curl http://0.0.0.0:8080/weather-forecast
```

For the first request it will respond with default weather value `sunny`.

To test `POST` method use following command:
```
curl -d "very hot" http://0.0.0.0:8080/weather-forecast
```
It will update value of `weather` variable and will respond with the message: `Weather has been set to 'very hot'`.

After that `GET` method will respond with new value of `weather` variable.
