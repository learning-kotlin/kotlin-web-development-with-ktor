package org.elu.learning.kotlin.ktor

import io.ktor.application.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*

fun Route.books() {
    val dataManager = DataManager()
    route("/books") {
        get("/") {
            call.respond(dataManager.allBooks())
        }

        post("/") {
            val book = call.receive(Book::class)
            val newBook = dataManager.newBook(book)
            call.respond(newBook)
        }

        put("/{id}") {
            val book = call.receive(Book::class)
            val updatedBook = dataManager.updateBook(book)
            updatedBook?.let {  call.respond(it) }
        }

        delete("/{id}") {
            val id = call.parameters["id"]
            val deletedBook = dataManager.deleteBook(id)
            deletedBook?.let {  call.respond(it) }
        }
    }
}
