package org.elu.learning.kotlin.ktor

class DataManager {
    private var books = ArrayList<Book>()

    private fun gimmeId(): String = books.size.toString()

    init {
        books.add(Book(gimmeId(), "How to grow apples", "Mr. Appleton", 100.0f))
        books.add(Book(gimmeId(), "How to grow oranges", "Mr. Orangeton", 90.0f))
        books.add(Book(gimmeId(), "How to grow lemons", "Mr. Lemon", 110.0f))
        books.add(Book(gimmeId(), "How to grow pineapples", "Mr. Pineapple", 100.0f))
        books.add(Book(gimmeId(), "How to grow pears", "Mr. Pears", 110.0f))
        books.add(Book(gimmeId(), "How to grow coconuts", "Mr. Coconut", 130.0f))
        books.add(Book(gimmeId(), "How to grow bananas", "Mr. Appleton", 120.0f))
    }

    fun newBook(book: Book): Book {
        books.add(book)
        return book
    }

    fun updateBook(book: Book) : Book? {
        val foundBook = books.find { it.id == book.id }
        foundBook?.title = book.title
        foundBook?.author = book.author
        foundBook?.price = book.price
        return foundBook
    }

    fun deleteBook(bookId: String?) : Book? {
        val book = books.find { it.id == bookId }
        books.remove(book)
        return book
    }

    fun allBooks() : List<Book> {
        return books
    }
}
